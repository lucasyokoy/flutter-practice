import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';
import './chart_bar.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions);

  List<Map<String, Object>> get groupedTransactionValues {
    //.generate is a method available for all lists. It applies a function on a given List, and returns a new List
    //the first argument is the size of the returned list. The secodn is the function, whose argument is the index of elements in the list it's applied on
    return List.generate(7, (index) {
      //the DateTime Object allows for date operations as below
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );
      double totalSum = 0.0;

      for (var i = 0; i < recentTransactions.length; i++) {
        if (recentTransactions[i].date.day == weekDay.day &&
            recentTransactions[i].date.month == weekDay.month &&
            recentTransactions[i].date.year == weekDay.year) {
          totalSum += recentTransactions[i].amount;
        }
      }
      return {
        'day': DateFormat.E().format(weekDay),
        'amount': totalSum,
      };
    }).reversed.toList();
    //Reverses the list. Otherwise it'll show the current day on the left, and the previous days from left to right.
  }

  double get maxSpending {
    //equivalent to the reduce function from python, this calculates the total value spended on the week
    //the second argument is the function to be applied. The first argument of that function is the value to be returned. The second is the item from the list
    //the first argument is the initial value of sum
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //MediaQuery behaves like in CSS. Here, it takes the height of the device and mutiplies by 0.6
      height: MediaQuery.of(context).size.height * 0.4,
      child: Card(
        elevation: 6,
        margin: EdgeInsets.all(20),
        //the Padding widget is like the Container, except it only has the padding property.
        //it's convenient if you would wrap something with a container only to set some padding.
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: groupedTransactionValues.map(
              (data) {
                return 
                //Works like flexbox from CSS.
                Flexible(
                  fit: FlexFit.tight,
                  child: ChartBar(
                    data['day'],
                    data['amount'],
                    maxSpending == 0
                        ? 0.0
                        : (data['amount'] as double) / maxSpending,
                  ),
                );
              },
            ).toList(),
          ),
        ),
      ),
    );
  }
}
