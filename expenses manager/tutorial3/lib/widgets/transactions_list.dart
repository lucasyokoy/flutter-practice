import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';
import './no_transactions.dart';

class TransactionsList extends StatelessWidget {
  final List<Transaction> userTransactions;
  final Function deleteTransaction;
  TransactionsList(this.userTransactions, this.deleteTransaction);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: userTransactions.isEmpty
          ? NoTransactions()
          : ListView.builder(
              itemCount: userTransactions.length,
              itemBuilder: (ctx, index) {
                //you can use the command Refactor... -> Extract Widget to automatically turn a selected chunk of code into a separate widget class
                return TransactionItem(userTransactions: userTransactions, deleteTransaction: deleteTransaction, index: index);
              },
            ),
    );

    //An alternative to ListTile would be a card and a row, as done below:
    //         Card(
    //       child: Row(
    //         children: <Widget>[
    //           Container(
    //             margin: EdgeInsets.symmetric(
    //               vertical: 10,
    //               horizontal: 15,
    //             ),
    //             decoration: BoxDecoration(
    //               border: Border.all(
    //                 width: 3.0,
    //                 color: Colors.black,
    //               ),
    //             ),
    //             padding: EdgeInsets.all(5.0),
    //             child: Text(
    //               //like an "f string" from python. $ is a special character for inserting variables into the text
    //               '\$${userTransactions[index].amount.toStringAsFixed(2)}',
    //               style: TextStyle(
    //                 fontWeight: FontWeight.bold,
    //                 fontSize: 20,
    //                 color: Colors.pink,
    //               ),
    //             ),
    //           ),
    //           Expanded(
    //             child: Column(
    //               crossAxisAlignment: CrossAxisAlignment.start,
    //               children: <Widget>[
    //                 Text(
    //                   userTransactions[index].title,
    //                   style: Theme.of(context).textTheme.title,
    //                 ),
    //                 Text(
    //                   DateFormat('dd-MM-yyyy')
    //                       .format(userTransactions[index].date),
    //                   style: TextStyle(
    //                     color: Colors.grey,
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //           IconButton(
    //             alignment: Alignment.centerRight,
    //             icon: Icon(Icons.delete),
    //             color: Theme.of(context).errorColor,
    //             onPressed: () {
    //               deleteTransaction(userTransactions[index].id);
    //             },
    //           )
    //         ],
    //       ),
    //     );
    //   },
    // ));
  }
}

class TransactionItem extends StatelessWidget {
  const TransactionItem({
    Key key,
    @required this.userTransactions,
    @required this.deleteTransaction,
    @required this.index,
  }) : super(key: key);

  final int index;
  final List<Transaction> userTransactions;
  final Function deleteTransaction;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 5,
      ),
      child: ListTile(
        leading: CircleAvatar(
          radius: 30,
          child: Padding(
            padding: EdgeInsets.all(6),
            child: FittedBox(
              child: Text('\$${userTransactions[index].amount}'),
            ),
          ),
        ),
        title: Text(
          userTransactions[index].title,
          style: Theme.of(context).textTheme.title,
        ),
        subtitle: Text(
          DateFormat.yMMMd().format(userTransactions[index].date),
        ),
        //Depending on how much space there is available, it either shows just the delete icon or the delete icon with a "Delete" text.
        //This depends on the size of the screen of the device, and whether it's on landscape or portrait mode
        trailing: MediaQuery.of(context).size.width > 360
            ? FlatButton.icon(
                onPressed: () => deleteTransaction(userTransactions[index].id),
                icon: Icon(Icons.delete),
                label: Text('Delete'),
                textColor: Theme.of(context).errorColor,
                )
            : IconButton(
                icon: Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                onPressed: () =>
                    deleteTransaction(userTransactions[index].id),
              ),
      ),
    );
  }
}
