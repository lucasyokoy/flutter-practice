import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  //Input variables are always strings. If you want a different type, convert it manually later
  //Using variables is one way of fetching input
  final Function newTransaction;

  NewTransaction(this.newTransaction);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  String titleInput;
  final _amountController = TextEditingController();
  DateTime _selectedDate;

  void _submitTransaction() {
    final providedTitle = titleInput;
    final providedAmount = double.parse(_amountController.text);
    final providedDate = _selectedDate;

    if (providedTitle.isEmpty || providedAmount <= 0 || providedDate == null) {
      return;
    }
    //This allows you to access functions from the widget from the state
    //this is usually automatically generated once you try to use a widget function from the state
    widget.newTransaction(providedTitle, providedAmount, providedDate);

    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now()
          .year), //The standard DateTime constructor uses positional arguments. The year is required. By default, all other arguments are set to the first (day, or month, or hour, etc)
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      //This function is executed by the Widget as soon as it's closed
      if (pickedDate == null) {
        //this would mean the user didn't pick any date, and instead just closed the window
        return;
      } else {
        setState(() {
          //if you want something to trigger a rebuild of a widget, remember to wrap it around a setState()
          _selectedDate = pickedDate;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      //In case the virtual keyboard overlaps with something on the form, you can now scroll it down.
      SingleChildScrollView(
      //TextField doesn't provide much styling on it's own. Wraping it in a card and a container allows for more styling flexibility.
      child: Card(
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                //onChanged calls the given funcion whenever the value in the textfields is changed
                onChanged: (newValue) => titleInput = newValue,
                decoration: InputDecoration(
                  labelText: 'Title',
                ),
              ),
              TextField(
                keyboardType: TextInputType.number,
                controller: _amountController,
                decoration: InputDecoration(
                  labelText: 'Amount',
                ),
                onSubmitted: (_) => _submitTransaction(),
              ),
              Container(
                height: 70,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        _selectedDate == null
                            ? 'No Date Chosen!'
                            : DateFormat.yMd().format(_selectedDate),
                      ),
                    ),
                    FlatButton(
                      textColor: Theme.of(context).primaryColor,
                      onPressed: _presentDatePicker,
                      child: Text(
                        'Choose a Date',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              RaisedButton(
                textColor: Theme.of(context).textTheme.button.color,
                color: Theme.of(context).primaryColor,
                onPressed: _submitTransaction,
                child: Text(
                  'Add Transaction',
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
