import 'package:flutter/material.dart';

class NoTransactions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx,constraints) {
      return Column(
        children: <Widget>[
          Text('No Transactions to be displayed.',
              style: Theme.of(context).textTheme.title),
          //SizedBox squeezed some empty space between the text and the image.
          SizedBox(
            height: 10,
          ),
          Container(
              height: constraints.maxHeight * 0.6,
              child: Image.asset(
                'assets/images/waiting.png',
                //fit parameter makes the image fit in it's immediate parent widget.
                //Because it was a column, which has intinite height, it was convenient to wrap it in a container with finite height.
                fit: BoxFit.contain,
              )),
        ],
      );
    });
  }
}
