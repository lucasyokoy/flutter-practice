//import 'dart:io';
//this import would allow you to figure out what platform you are on. And just as you can adapt the interface for different device sizes, you can also adapt it for different platforms.
//exemple: Platform.iOS is a boolean that returns true if the platform it's running on is iOS
//this can be used in ternary expresions to make adaptative interfaces.

import 'package:flutter/material.dart';
import 'package:tutorial3/widgets/transactions_list.dart';
// import 'package:flutter/services.dart';

import './widgets/new_transaction.dart';
import './models/transaction.dart';
import './widgets/transactions_list.dart';
import './widgets/chart.dart';

void main() {
  //The following commented lines only allow the device to run on portrait mode. Not on Landscape mode
  //Must import 'package:flutter/services.dart'; to work
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations([
  //   DeviceOrientation.portraitUp,
  //   DeviceOrientation.portraitDown,
  // ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.green,
        errorColor: Colors.red,
        fontFamily: 'QUickSand',
        textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                button: TextStyle(color: Colors.white),
              ),
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  //shows the text fields for inputting new entries on the purchase list
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

//the app lifecycle is it's state. Whether it's paused, closing, inactive, active, etc.
//the state, and changes thereof, can be tracked if you extend the state class like:
//class _MyHomePageState extends State<MyHomePage> with WidgetBindingObserver
//the with keyword can be used to "kind of" inherit characteristics from multiple classes.
class _MyHomePageState extends State<MyHomePage> {
  final List<Transaction> _userTransactions = [
    Transaction(
      id: '0',
      title: 'New Shoes',
      amount: 20.00,
      date: DateTime.now().subtract(Duration(days: 2)),
    ),
    Transaction(
      amount: 10.00,
      id: '1',
      title: 'Carrot Sharpener',
      date: DateTime.now(),
    ),
  ];

  bool _showChart = false;

  //get methods can be called and used like variables. But instead of simply outputting a value, they execute a function that returns something.
  List<Transaction> get _recentTransactions {
    //where is a method of the List class.
    //It returns a new list and applies a function to each element in the list it's applied on. If the function returns true, the element is included in the returned list
    //like a where clause in SQL.
    return _userTransactions.where((tx) {
      //returns true if the date of the transaction in the list of transactions is after 7 days ago.
      return tx.date.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _newTransaction(String title, double amount, DateTime date) {
    final newTx = Transaction(
      amount: amount,
      id: DateTime.now().toString(),
      title: title,
      date: date,
    );
    setState(() {
      _userTransactions.add(newTx);
    });
  }

  void _startAddNewTransaction(BuildContext ctx) {
    showModalBottomSheet(
      context: ctx,
      builder: (_) {
        return GestureDetector(
          //onTap: () {}, means nothing will happen when the user touches on the field
          onTap: () {},
          //Text Fields for new transactions
          child: NewTransaction(_newTransaction),
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  void _deleteTransaction(String txId) {
    setState(() {
      _userTransactions.removeWhere((tx) {
        return tx.id == txId;
      });
    });
  }

  //initState is called when the widget is first instantiated
  //often used to fetch data via http requests to load on the screen after the app is opened
  @override
  void initState() {
    super.initState();
    print('initState()');
  }

  //didUPdateWidget is called after the widget is updated. Either by itself or by it's parent widget
  @override
  void didUpdateWidget(MyHomePage oldWidget) {
    print('didUpdateWidget');
    super.didUpdateWidget(oldWidget);
  }

  //dispose is called when the widget is supposed to disappear (the render tree must be deleted)
  @override
  void dispose() {
    print('dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final isLandscape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    final AppBar appBar = AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      //IMPORTANT: using const methods and widgets can improve performance because they don't require the rebuilding of the widget tree.
      //however, widgets can only be const if all of their parameters are constants, and never change.
      title: const Text(
        'Flutter App',
        style: TextStyle(
          fontFamily: 'OpenSans',
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          //important: the difference between onPressed: null, and onPressed: () {},
          //in the former the button is disabled. In the later, it's enabled but does nothing when pressed
          onPressed: () => _startAddNewTransaction(context),
        ),
      ],
    );

    //transactions list
    final Widget _transactionsListWidget = Container(
      //here, 70% of (size_of_the_screen - size_of_appbar - size_of_top_bar) is reserved for the chart
      height: (MediaQuery.of(context).size.height -
              appBar.preferredSize.height -
              MediaQuery.of(context).padding.top) *
          1,
      child: TransactionsList(_userTransactions, _deleteTransaction),
    );

    //Chart builder
    Widget _transactionsChart(double size) {
      return Container(
        //this subtracts the total height of the screen from the height of the appbar and of the top bar so everything fits on the screen
        //here, 30% of (size_of_the_screen - size_of_appbar - size_of_top_bar) is reserved for the chart
        height: (MediaQuery.of(context).size.height -
                appBar.preferredSize.height -
                MediaQuery.of(context).padding.top) *
            size,
        child: Chart(_recentTransactions),
      );
    }

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //main axis: larger(usually vertical when upright), cross axis: smaller(horizontal when upright). Regardless of the orientation of the phone
          //stretch stretches up all the child widgets to the size of the cross axis of the father widget (the column).
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //with this if() the chart switch only appears when the device is in landscape position
            if (isLandscape)
              Row(
                children: <Widget>[
                  Text('Show Chart'),
                  Switch(
                    value: _showChart,
                    //Switch makes a switch that toggles something. The onChanged function receives the state of the switch (true or false, on or off), and performs a function when the state is changed
                    onChanged: (val) {
                      setState(() {
                        _showChart = val;
                      });
                    },
                  ),
                ],
              ),
            //if the device is not in landscape, show both the transaction chart and the transactions list.
            //if it is in landscape, show one or the other, depending whether the show Chart widget is toggled or not.
            //this can also be used to render different content based on the orienttion.
            if (!isLandscape)
              _transactionsChart(0.3),
            if (!isLandscape)
              _transactionsListWidget,
            //thanks to this ternary expression, the switch chooses between showing the purchase chart or the list of purchases
            if (isLandscape)
              _showChart ? _transactionsChart(0.7) : _transactionsListWidget,
          ],
        ),
      ),
      //floatingActionButton is a parameter from Scaffold. And so is floatingActionButtonLocation
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _startAddNewTransaction(context),
      ),
    );
  }
}
