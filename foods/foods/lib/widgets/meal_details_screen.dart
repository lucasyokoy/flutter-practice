import 'package:flutter/material.dart';

import '../models/dummy-data.dart';

class MealDetailsScreen extends StatelessWidget {
  static const routeName = '/widgets/meal_details_screen';

  final Function toggleFavorite;
  final Function isFavorite;

  MealDetailsScreen(this.toggleFavorite, this.isFavorite);

  Widget buildHeader(BuildContext ctx, String header) {
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      child: Text(
        header,
        style: Theme.of(ctx).textTheme.title,
      ),
    );
  }

  Widget buildListContainer(Widget child) {
    //a ListView has takes as much height as it can. A Column has infinite height. These 2 combined would throw and error
    //for that reason, the ListView should be wraped around by a Container if it is to be placed in a Column
    return Container(
      margin: EdgeInsets.only(
        bottom: 10,
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.grey,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(10)),
      height: 200,
      width: 300,
      //As seen before, ListView.builder() has better performance than ListView() when the amount of items is unknown or very large
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments as String;
    //firstWhere stops the search as soon as it finds one match. For searching a single element, this is a lot faster. Also, it returns a Meal instead of an Iterable<Meal>
    final selectedMeal = DUMMY_MEALS.firstWhere((meal) {
      return meal.id == id;
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedMeal.title}', overflow: TextOverflow.fade),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed:()=> toggleFavorite(id),
        //() {
        //   //pop() pops the Widget stack, revealing the previously visible page/widget
        //   //canPop() is a boolean that returns ture if there is a page to return to, if the stack is popped. It might be a good idea to check before popping the stack
        //   //.pop(popArg) can receive an argument that is sent to the .then() function from ./category_item.dart
        //   Navigator.of(context).pop(id);
        // },
        child: Icon(
          isFavorite(id) ? Icons.star : Icons.star_border,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                selectedMeal.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildHeader(context, 'Ingredients'),
            buildListContainer(
              ListView.builder(
                itemCount: selectedMeal.ingredients.length,
                itemBuilder: (context, index) {
                  return Card(
                    color: Theme.of(context).accentColor,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 15,
                      ),
                      child: Text(selectedMeal.ingredients[index]),
                    ),
                  );
                },
              ),
            ),
            buildHeader(context, 'Steps:'),
            buildListContainer(
              ListView.builder(
                itemCount: selectedMeal.steps.length,
                itemBuilder: (ctx, index) => Column(
                  children: [
                    ListTile(
                      leading: CircleAvatar(
                        //index+1 because lists begin with 0. Remember to wrap it in paranthesis
                        child: Text('# ${(index + 1)}'),
                      ),
                      title: Text(
                        selectedMeal.steps[index],
                      ),
                    ),
                    //Divider() inserts thin vertical lines separating the widgets in a Column or Row
                    Divider(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
