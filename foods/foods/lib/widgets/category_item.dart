import 'package:flutter/material.dart';

import '../screens/category_meals_screen.dart';

class CategoryItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  CategoryItem(this.id, this.title, this.color);

  void selectCategory(BuildContext ctx) {
    //Flutter treats the different screen as a stack. The navigator can be used to pop or push elements into this stack.
    //The element on top is seen on the screen. Pressing the back button also pops the element on top of the stack
    // Navigator.of(ctx).push(
    //   MaterialPageRoute(
    //     builder: (_) {
    //       return CategoryMealsScreen(id, title);
    //     },
    //   ),
    // );

    //use pushNamed in case you are using a routes list in main
    Navigator.of(ctx).pushNamed(
      CategoryMealsScreen.routeName,
      arguments: {'id': id, 'title': title},
    );
  }

  @override
  Widget build(BuildContext context) {
    //Container doesn't work as a button. There is no onTap function to be set.
    //If you want a non-button widget to behave like a button, you can wrap it on an InkWell or a GestureDetector
    //Besies turning into a button, ink well also provides visual effects on tap like the ripple effect.
    return InkWell(
      onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(10),
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Text(title, style: Theme.of(context).textTheme.title),
      ),
    );
  }
}
