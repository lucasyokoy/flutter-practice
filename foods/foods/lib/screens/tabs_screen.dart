import 'package:flutter/material.dart';
import 'package:foods/screens/favorites_screen.dart';

import '../widgets/main_drawer.dart';
import '../screens/categories_screen.dart';
import '../models/meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favoriteMeals;

  TabsScreen(this.favoriteMeals);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  @override
  Widget build(BuildContext context) {
    //DefaultTabController manages the entire screen, like Scaffold. So that the Widgets it wraps don't need a Scaffold
    return DefaultTabController(
      length: 2,
      //initialIndex defines which tab will be opened by default, starting at 0, with default 0
      initialIndex: 0,
      child: Scaffold(
        //the drawer is a panel you can open by swiping on the side of the screen towards the center
        drawer: MainDrawer(),
        appBar: AppBar(
          title: Text('Meals'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(icon: Icon(Icons.category), text: 'Categories'),
              Tab(icon: Icon(Icons.star), text: 'Favorites'),
            ],
          ),
        ),
        //TabBarView should have as many children as there are tabs in DefaultTabController(lenght:
        body: TabBarView(
          children: [
            CategoriesScreen(),
            FavoritesScreen(widget.favoriteMeals),
          ],
        ),
      ),
    );
  }
}
