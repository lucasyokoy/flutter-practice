import 'package:flutter/material.dart';

import '../widgets/meal_item.dart';
import '../models/meal.dart';

class CategoryMealsScreen extends StatelessWidget {
  // final String categoryId;
  // final String categoryTitle;

  // CategoryMealsScreen(this.categoryId,this.categoryTitle);

  //static properties can be accessed without instantiating that class.
  //this can be useful if you want the name of the route of the class to be available in the route table, for example
  static const routeName = '/screens/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  Widget build(BuildContext context) {
    //these variables are useful when using route tables on main
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final categoryTitle = routeArgs['title'];
    final categoryId = routeArgs['id'];
    //where only selects the elemens in the list where the function returns true (like in SQL)
    final categoryMeals = availableMeals.where((meal) {
      //returns true if the categories (List<String>) contains the desired category
      return meal.categories.contains(categoryId);
    }).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemCount: categoryMeals.length,
        itemBuilder: (ctx, index) {
          return MealItem(
            id: categoryMeals[index].id,
            title: categoryMeals[index].title,
            imageUrl: categoryMeals[index].imageUrl,
            affordability: categoryMeals[index].affordability,
            complexity: categoryMeals[index].complexity,
            duration: categoryMeals[index].duration,
          );
        },
      ),
      //bottomNavigationBar is like DefaultTabController from tab_sceen.dart. Except that the tabs are at the bottom
      //onTap must activate a function that changes the body of the scaffold. Therefore the body:... must be replaced with body: _tabSelector(_tabIndex)
      //and onTap: must call a function like selectState(setNewTabIndex);
      //for more information, visit nav-10-bottom-tabs project folder
      bottomNavigationBar: BottomNavigationBar(
         onTap: null,
         //type: shifting only shows the text for the selected item.
         type: BottomNavigationBarType.shifting,
         unselectedItemColor: Colors.white,
         selectedItemColor: Theme.of(context).accentColor,
         items: [
           BottomNavigationBarItem(
             backgroundColor: Theme.of(context).primaryColor,
             icon: Icon(Icons.category),
             title: Text('Categories'),
           ),
           BottomNavigationBarItem(
             backgroundColor: Theme.of(context).primaryColor,
             icon: Icon(Icons.star),
             title: Text('Favorites'),
           ),
         ],
       ),
      // BottomNavigationBar(
      //   onTap: (_){},
      //   backgroundColor: Theme.of(context).primaryColor,
      //   unselectedItemColor: Colors.white,
      //   selectedItemColor: Theme.of(context).accentColor,
      //   // type: BottomNavigationBarType.fixed,
      //   items: [
      //     BottomNavigationBarItem(
      //       backgroundColor: Theme.of(context).primaryColor,
      //       icon: Icon(Icons.category),
      //       title: Text('Categories'),
      //     ),
      //     BottomNavigationBarItem(
      //       backgroundColor: Theme.of(context).primaryColor,
      //       icon: Icon(Icons.star),
      //       title: Text('Favorites'),
      //     ),
      //   ],
      // ),
    );
  }
}
