import 'package:flutter/material.dart';

import './models/dummy-data.dart';
import './screens/filters_screen.dart';
import './widgets/meal_details_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/tabs_screen.dart';
import './models/meal.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };
  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  void _setFilters(Map<String, bool> filteredData) {
    setState(() {
      _filters = filteredData;
      //if(condition) return false; is a more efficient logic because it doesn't need to check wether or not the meal satisfies multiple filters.
      //If it DOESN'T satisfy a filter, it's out of the list.
      _availableMeals = DUMMY_MEALS.where((meal) {
        if (_filters['gluten'] && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    //indexWhere returns the index of an element in the list, where the given function returns true. If there is no such element in the list, it returns -1
    final isFavorite = _favoriteMeals.indexWhere((meal) => meal.id == mealId);
    if (isFavorite >= 0) {
      setState(() {
        _favoriteMeals.removeAt(isFavorite);
      });
    } else {
      setState(() {
        _favoriteMeals.add(
          DUMMY_MEALS.firstWhere(
            (meal) {
              return meal.id == mealId;
            },
          ),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    //any returns true if there is any element in the list that satisfies the function
    return _favoriteMeals.any(
      (meal) {
        return meal.id == id;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amberAccent,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              body2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              title: TextStyle(
                fontSize: 24,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      home: TabsScreen(_favoriteMeals),
      //a routes table can make it easier to see what pages can be accessed from this one. Especially in larger apps
      routes: {
        // '/':(ctx) => CategoriesScree(), would be equivalent to having a home: CategoriesScreen(), parameter as above.
        CategoryMealsScreen.routeName: (ctx) =>
            CategoryMealsScreen(_availableMeals),
        MealDetailsScreen.routeName: (ctx) =>
            MealDetailsScreen(_toggleFavorite, _isMealFavorite),
        FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
      },
      //onGenerateRoute can be used to redirect the user to dynamically generated routes.
      //onUnknownRoute can be used to handle requests to unknown routes
      //onGenerateRoute is reached when a pushRoute requests a route not found in the routes: parameter above
      //onUnknownRoute is reached when neither routes: nor onGeneratedRoute can handle the request. It usually returns an error page (like 404).
      //if it wasn't for them the app would cash if routes couldn't handle a request
    );
  }
}
