import 'package:flutter/material.dart';

import './products.dart';
import './product_control.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  
  ProductManager({this.startingProduct = 'Sweet Tester'});
  
  @override
  State<StatefulWidget> createState() {
    //TODO: implement createState
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  void _addProduct(String newProduct){
    setState(() {
      _products.add(newProduct);
    });
  }

  @override
  void initState() {
    _products.add(widget.startingProduct);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children:[Container(
      margin: EdgeInsets.all(10.0),
      child: ProductControl(_addProduct),
      ),
      Products(_products)
    ],);
  }
}
