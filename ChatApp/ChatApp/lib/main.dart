import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import './screens/auth_screen.dart';
import './screens/chat_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Chat',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        backgroundColor: Colors.pink,
        accentColor: Colors.deepPurple,
        accentColorBrightness: Brightness.dark,
        //ButtonTheme.of.copyWith() copies all properties form the default button, but overrides the specified ones
        buttonTheme: ButtonTheme.of(context).copyWith(
          buttonColor: Colors.pink,
          textTheme: ButtonTextTheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          ),
        ),
      ),
      //onAuthStateChanged will return a stream that changes when you log in our out
      home: StreamBuilder(stream: FirebaseAuth.instance.onAuthStateChanged, builder:(ctx, userSnapshot){
        if(userSnapshot.hasData){
          return ChatScreen();
        }
        //Notice that, if you were logged in before the app runs, the login screen appears for a split second before the chat screen.
        //That's because firebase takes a while to responde to the app whether the user is authenticated or not.
        //This could be fixed with a 'progress' screen after login, but it's not a major issue
        return AuthScreen();
      } ,),
    );
  }
}

