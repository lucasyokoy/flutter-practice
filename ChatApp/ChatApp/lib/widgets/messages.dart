import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import './message_bubble.dart';

class Messages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: FirebaseAuth.instance.currentUser(),
      builder: (ctx, futureSnapshot) {
        if (futureSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }
        return StreamBuilder(
          //orderBy makes sure the messages are ordered by timestamp.
          stream: Firestore.instance
              .collection('chat')
              .orderBy('timestamp', descending: true)
              .snapshots(),
          builder: (ctx, chatSnapshot) {
            if (chatSnapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            final chatDocs = chatSnapshot.data.documents;
            //renders chat messages as they are resolved from the .currentUser() future
            return ListView.builder(
              //reverse:true, makes sure the last sent message is shown at the bottom.
              reverse: true,
              itemCount: chatDocs.length,
              itemBuilder: (ctx, index) => MessageBubble(
                key: ValueKey(chatDocs[index].documentID),
                textMessage: chatDocs[index]['text'],
                username: chatDocs[index]['username'],
                userImage: chatDocs[index]['userImage'],
                isMe: chatDocs[index]['userId'] == futureSnapshot.data.uid,
              ),
            );
          },
        );
      },
    );
  }
}
