import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../widgets/messages.dart';
import '../widgets/new_message.dart';

class ChatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FlutterChat'),
        actions: [
          DropdownButton(
            //without this empty container, there will be an ugly grey line dividing the appBar: and the body: of the Scaffold
            underline: Container(),
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            items: [
              DropdownMenuItem(
                child: Container(
                  child: Row(
                    children: [
                      Icon(Icons.exit_to_app),
                      SizedBox(width: 8),
                      Text('Logout'),
                    ],
                  ),
                ),
                value: 'logout',
              ),
            ],
            onChanged: (itemIdentifier) {
              if (itemIdentifier == 'logout') {
                FirebaseAuth.instance.signOut();
              }
            },
          ),
        ],
      ),
      //the StreamBuilder will rebuild the builder function whenever the Stream generates some new data
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Messages(),
            ),
            NewMessage(),
          ],
        ),
      ),
      // StreamBuilder(
      //   stream:
      //       //.collection() this accesses a collection in the firebase database: 'chats/collection-id
      //       //.snapshot() returns a stream. Which re-renders the widget in real time whenever there is a change on the data from server, giving a real time feeling to the code
      //       Firestore.instance
      //           .collection('chats/l5gQuWWCrHQtokw0EtvW/messages')
      //           .snapshots(),
      //   builder: (ctx, streamSnapshot) {
      //     //this if is needed to avoid the following error: when the app loads for the first time, there is no data on the stream, therefore itemCount == 0.
      //     if (streamSnapshot.connectionState == ConnectionState.waiting) {
      //       return Center(child: CircularProgressIndicator());
      //     }
      //     final documents = streamSnapshot.data.documents;
      //     return ListView.builder(
      //       itemCount: documents.length,
      //       itemBuilder: (ctx, index) => Container(
      //         padding: EdgeInsets.all(8),
      //         child: Text(documents[index]['text']),
      //       ),
      //     );
      //   },
      // ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.add),
      //   onPressed: () {
      //     //.add() adds a new document to the database. See documentation for further intormation
      //     Firestore.instance
      //         .collection('chats/l5gQuWWCrHQtokw0EtvW/messages')
      //         .add(
      //       {
      //         'text': 'Added by user',
      //       },
      //     );
      //   },
      // ),
    );
  }
}
