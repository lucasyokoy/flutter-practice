const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
// // After creating the function, you must deploy it to the firebase server using the command $ firebase deploy
//
// // you can use this file to allow for push in notifications on all devices whenever someone sends a message to the chat.
// // the notification will contain the message thats been sent.
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.myFunction = functions.firestore
    .document('chat/{message}')
    .onCreate((snapshot, context) => {
        admin.messaging().sendToTopic('chat',{notification: {
            title: snapshot.data().username,
            body: snapshot.data().text,
            clickAction: 'FLUTTER_NOTIFICATION_CLICK',
        }});

    });