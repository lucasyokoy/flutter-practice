import 'dart:convert';

import 'package:http/http.dart' as http;

const GOOGLE_API_KEY = 'AIzaSyCd18ocN2uNGzv4RZ4UXjq4R3vqTdDpHtk';

class LocationHelper{
  //this URL generates a snapshop picture of a location in google maps. For source, search for Google Maps Static API
  static String generateLocationPreviewImage({double latitude, double longitude}){
    return 'https://maps.googleapis.com/maps/api/staticmap?center=&$latitude,$longitude&zoom=16&size=600x300&maptype=roadmap&markers=color:red%7Clabel:A%7C$latitude,$longitude&key=$GOOGLE_API_KEY';
  }

  static Future<String> getPlaceAddress(double lat, double lng) async {
    	final url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=$GOOGLE_API_KEY';
      final response = await http.get(url);
      //decodes the response body and returns it (read documentation for more information )
      return json.decode(response.body)['results'][0]['formatted_address'];
  }
}