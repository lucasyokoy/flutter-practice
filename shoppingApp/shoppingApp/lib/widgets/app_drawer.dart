import 'package:flutter/material.dart';
import 'package:shoppingApp/screens/user_products_screen.dart';
import 'package:provider/provider.dart';

import '../providers/auth_provider.dart';
import '../screens/orders_screen.dart';
import '../screens/user_products_screen.dart';
import '../helpers/custom_route.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text('Hello friend'),
            automaticallyImplyLeading: false,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('Shop'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Manage Products'),
            onTap: () {
              Navigator.of(context).pushNamed(UserProductsScreen.routeName);
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('Orders'),
            onTap: () {
              // Navigator.of(context).pushReplacementNamed(OrdersScreen.routeName);
              //with the custom route, it can now play the Fade animation when transitioning to the orders screen
              //this applies a given transition to one single place.
              Navigator.of(context).pushReplacement(CustomRoute(
                builder: (ctx) => OrdersScreen(),
              ));
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () {
              //without the stack pop, there will be an error because the drawer will still be opened when the user logs out. That sudden switch of widgets causes an error.
              Navigator.of(context).pop();
              //this ensures there will be no exepcted behavior after logging out, and the user will always end up in the homescreen after logging out.
              Navigator.of(context).pushReplacementNamed('/');
              Provider.of<AuthProvider>(context).logout();
            },
          ),
        ],
      ),
    );
  }
}
