import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/product_detail_screen.dart';
import '../providers/product.dart';
import '../providers/cart.dart';
import '../providers/auth_provider.dart';

class ProductItem extends StatelessWidget {
  // final String id;
  // final String title;
  // final String imageUrl;

  // const ProductItem(
  //   this.id,
  //   this.title,
  //   this.imageUrl,
  // );

  @override
  Widget build(BuildContext context) {
    //this listener will rebuild the widget as soon as the notifyListeners() method from products.dart is called. Just like all other widgets listening to the same provider
    //if listen: false, this particular widget would ignore the notifyListeners() call and not rebuild
    //an alternative syntax would be Consumer<Product> wraping around ClipRRect
    //Consumer uses a build: (ctx, product, child) => *ClipRRect Widget Tree etc* parameter
    //ctx is an internal context, product is the data passed to the builder, child is an immutable widget passed on by an argument as in Consumer<Product>{builder:...,child:...,}
    //child can then be referenced within the builder, and it won't be rebuilt when the Consumer is called. Thus improving efficiency and flexibility fo the widget rebuilding.
    //Consumer and Provider.of can co-exist. For example, if listen: false in provider, you can still wrap a part of the provider in a Consumer and get that part changed, even tought the widget as a whole won't change
    final product = Provider.of<Product>(
      context,
      listen: false,
    );
    final cart = Provider.of<Cart>(
      context,
      listen: false,
    );
    final auth = Provider.of<AuthProvider>(
      context,
      listen: false,
    );

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: product.id,
            );
          },
          //the Hero widget makes the image expand when you tap on it
          //don't forget to also wrap the Hero widget with the same tag around the other widget where it flies into (product_detail_screen.dart)
          child: Hero(
            tag: product.id,
            //with the FadeInImage animation, the images smoothly fade in instead of suddenly popping up
            child: FadeInImage(
              placeholder: AssetImage('assets/images/product-placeholder.png'),
              image: NetworkImage(product.imageUrl),
              fit: BoxFit.cover,
            ),
          ),
          // Image.network(
          //   product.imageUrl,
          //   fit: BoxFit.cover,
          // ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          leading: Consumer<Product>(
            builder: (ctx, product, _) => IconButton(
              color: Theme.of(context).accentColor,
              icon: Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
              onPressed: () {
                product.toggleFavoriteStatus(
                    auth.tokenGetter, auth.userIdGetter);
              },
            ),
          ),
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            color: Theme.of(context).accentColor,
            icon: Icon(
              Icons.shopping_cart,
            ),
            onPressed: () {
              cart.addItem(
                product.id,
                product.price,
                product.title,
              );
              //Scaffold.of(context) establishes a connection with the nearest Scaffold.
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    'Added item to cart!',
                    textAlign: TextAlign.center,
                  ),
                  duration: Duration(
                    seconds: 2,
                  ),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      cart.removeSingleUnit(product.id);
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
