import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/edit_product.dart';
import '../providers/product.dart';
import '../providers/products_provider.dart';

class UserProductItem extends StatelessWidget {
  final Product productData;
  final Key productKey;

  UserProductItem(this.productKey, this.productData);

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold.of(context);
    return Column(
      children: [
        ListTile(
          title: Text(productData.title),
          leading: CircleAvatar(
            backgroundImage: NetworkImage(productData.imageUrl),
          ),
          trailing: Container(
            width: 100,
            child: Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.of(context).pushNamed(
                      EditProductScreen.routeName,
                      arguments: productData.id,
                    );
                  },
                  color: Theme.of(context).primaryColor,
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    //should be turned into async in order to work with the Future returned by deleteProduct()
                    try {
                      await Provider.of<ProductsProvider>(context,
                              listen: false)
                          .deleteProduct(productData.id);
                    } catch (error) {
                      //context cannot be used in a Future, because it doesn't know whether it should use the current context, or the Future's context. It must be specified.
                      Scaffold.of(scaffold.context).showSnackBar(
                        SnackBar(
                          content: Text(
                            'Deleting failed!',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    }
                  },
                  color: Theme.of(context).errorColor,
                ),
              ],
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}
