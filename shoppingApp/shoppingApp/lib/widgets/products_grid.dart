import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products_provider.dart';
import './product_Item.dart';

class ProductsGrid extends StatelessWidget {
  final bool showOnlyFavorites;

  ProductsGrid(this.showOnlyFavorites);

  @override
  Widget build(BuildContext context) {
    //listener for the provider from the main widget
    //only the widget listening will change when the widge rebuilds
    final productsData = Provider.of<ProductsProvider>(context);
    final loadedProducts = showOnlyFavorites ? productsData.favoriteItems : productsData.items;
    
    return GridView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: loadedProducts.length,
      //gridDelegate defines how the items should be distributed on the screen.
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      //This is an example of a situation where ChangeNotifierProvider.value is necessary, otherwise there will be bugs with ChangeNotifierProvider(create: )
      itemBuilder: (ctx, index) => ChangeNotifierProvider.value(
        value: loadedProducts[index],
        child: ProductItem(
          // loadedProducts[index].id,
          // loadedProducts[index].title,
          // loadedProducts[index].imageUrl,
        ),
      ),
    );
  }
}
// class ProductsGrid extends StatelessWidget {
//   final bool showFavs;

//   ProductsGrid(this.showFavs);

//   @override
//   Widget build(BuildContext context) {
//     final productsData = Provider.of<ProductsProvider>(context);
//     final products = showFavs ? productsData.favoriteItems : productsData.items;
//     return GridView.builder(
//       padding: const EdgeInsets.all(10.0),
//       itemCount: products.length,
//       itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
//             // builder: (c) => products[i],
//             value: products[i],
//             child: ProductItem(
//                 // products[i].id,
//                 // products[i].title,
//                 // products[i].imageUrl,
//                 ),
//           ),
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: 2,
//         childAspectRatio: 3 / 2,
//         crossAxisSpacing: 10,
//         mainAxisSpacing: 10,
//       ),
//     );
//   }
// }
