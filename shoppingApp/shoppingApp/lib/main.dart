import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/products_overview_screen.dart';
import './screens/product_detail_screen.dart';
import './screens/orders_screen.dart';
import './screens/cart_screen.dart';
import './screens/user_products_screen.dart';
import './screens/edit_product.dart';
import './screens/auth-screen.dart';
import './screens/splash-screen.dart';
import './providers/auth_provider.dart';
import './providers/products_provider.dart';
import './providers/cart.dart';
import './providers/orders.dart';
import './helpers/custom_route.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //providers are a tool to more efficiently transfer data from one widget to another.
    //providers can transfer data to any child (or child of child, or child of child of child, etc) widget with a listener
    //builder is an argument for version ^3.0.0 of provider
    //for later versions, use create: instead of build. It does the same thing tough.
    //this method provides data to the widgets which will be rebuilt when the build method is called again. So it doesn't have to rebuild the entire widget, only the ones that are listening.
    //an alternative syntax would be ChangeNotifierProvider.value(args...). Then, instead of create, use the value: parameter. This is useful if you're not using the ctx
    //the alternative is the right choice when you use a provider on something that is part of a list or a grid. Because the widgets are recycled by the flutter but the data changes.
    //use create if you are creating a new screen.
    //MultiProvider allows for easily nesting multiple provinders in a single widget.
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          builder: (ctx) => AuthProvider(),
        ),
        ChangeNotifierProxyProvider<AuthProvider, ProductsProvider>(
          //the second argument in the declaration (ProductsProvider) will be the type of data that will be provided.
          //the authProvider argument in the function is a dynamic value passed as an argumet to the builder. The buildr will rebuild when the provider from the reference (authProvider) is modified
          //the third argument in the function references the previous state, prior to this provider being called.
          //if there are more dependencies, you can use different versions of the constructor.
          builder: (ctx, authProvider, previousProducts) => ProductsProvider(
              authProvider.tokenGetter,
              authProvider.userIdGetter,
              previousProducts == null ? [] : previousProducts.items),
        ),
        ChangeNotifierProvider(
          builder: (ctx) => Cart(),
        ),
        ChangeNotifierProxyProvider<AuthProvider, Orders>(
          builder: (ctx, authProvider, previousOrders) => Orders(
            authProvider.tokenGetter,
            previousOrders == null ? [] : previousOrders.orders,
          ),
        ),
      ],
      child: Consumer<AuthProvider>(
        builder: (ctx, authData, _) => MaterialApp(
          title: 'MyShop',
          theme: ThemeData(
            primarySwatch: Colors.purple,
            accentColor: Colors.deepOrange,
            fontFamily: 'Lato',
            //pageTransitionsTheme allows you to change the transition animation for all the routes
            //you can set different transition themes for different OSs
            pageTransitionsTheme: PageTransitionsTheme(
              builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder(),
              },
            ),
          ),
          home: authData.isAuth
              //if the user is authenticated, show the ProductsOverviewScreen. Otherwise show the authentication screen.
              ? ProductsOverviewScreen()
              : FutureBuilder(
                  future: authData.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) {
                    //if the screen is loading, return the splash screen. After that, return the AuthScreen
                    if (authResultSnapshot.connectionState ==
                        ConnectionState.waiting) {
                      return SplashScreen();
                    } else {
                      return AuthScreen();
                    }
                  },
                ),
          routes: {
            ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
            CartScreen.routeName: (ctx) => CartScreen(),
            OrdersScreen.routeName: (ctx) => OrdersScreen(),
            UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
            EditProductScreen.routeName: (ctx) => EditProductScreen(),
          },
        ),
      ),
    );
  }
}
