import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  //this function inverts the status of isFavorite, and notifies all listeners using the provider package. All widgets with listeners will be rebuilt
  Future<void> toggleFavoriteStatus(String authToken, String userId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;
    notifyListeners();

    final databaseURL =
    //In case you are filtering user data by the URL, firebase has a built in filtering tool. Look for the documentation, or auth-06-attaching-products-to-users.zip file
        'https://flutter-tutorial-fb707.firebaseio.com/userFavorites/$userId/$id.json?auth=$authToken';
    try {
      //if the phone doesn't even connect to the internet, there won't even be an response, only a SocketException
      final response = await http.put(
        databaseURL,
        body: json.encode(
          isFavorite,
        ),
      );
      if (response.statusCode >= 400) {
        throw HttpException(
          'Failed to toggle favorite status of item $id.',
        );
      }
    } catch (error) {
      print('from favoriteToggle error catch ${error.toString()}');
      isFavorite = oldStatus;
      notifyListeners();
    }
  }
}
