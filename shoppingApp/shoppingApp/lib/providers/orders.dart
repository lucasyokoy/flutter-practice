import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';
import './cart.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    this.id,
    this.amount,
    this.products,
    this.dateTime,
  });
}

class Orders with ChangeNotifier {
  static final routeName = '/providers/orders';

  List<OrderItem> _orders = [];
  final String authToken;

  Orders(this.authToken, this._orders);

  List<OrderItem> get orders {
    return [..._orders];
  }

  Future<void> fetchOrders(String userId) async {
    final databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userOrders/$userId.json?auth=$authToken';
    try {
      final response = await http.get(databaseURL);
      if (response.statusCode >= 400) {
        throw HttpException('Failed to fetch orders from server');
      }
      final List<OrderItem> loadedOrders = [];
      final extractedData = json.decode(response.body);
      if (extractedData == null) {
        print(extractedData);
        _orders = [];
        return;
      }
      extractedData.forEach((orderId, orderdata) {
        loadedOrders.add(
          OrderItem(
            id: orderId,
            amount: orderdata['amount'],
            dateTime: DateTime.parse(orderdata['dateTime']),
            //if you know the type of a file, but the file doesn't, put it explicitly just for sure
            products: (orderdata['products'] as List<dynamic>)
                .map(
                  (item) => CartItem(
                    id: item['id'],
                    price: item['price'],
                    quantity: item['quantity'],
                    title: item['title'],
                  ),
                )
                .toList(),
          ),
        );
      });
      _orders = loadedOrders.reversed.toList();
      notifyListeners();
    } catch (error) {
      print(error.toString());
    }
  }

  Future<void> addOrder(List<CartItem> cartProducts, double total, String userId) async {
    final databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userOrders/$userId.json?auth=$authToken';
    final timeStamp = DateTime.now();
    try {
      final response = await http.post(databaseURL,
          body: json.encode({
            'amount': total,
            'dateTime': timeStamp.toIso8601String(),
            //this list of Maps can be converted into JSON
            'products': cartProducts
                .map((cartProduct) => {
                      'id': cartProduct.id,
                      'title': cartProduct.title,
                      'quantity': cartProduct.quantity,
                      'price': cartProduct.price,
                    })
                .toList(),
          }));
      if (response.statusCode >= 400) {
        throw HttpException('Failed to add Order');
      }
      _orders.insert(
        0,
        OrderItem(
          id: json.decode(response.body)['name'],
          amount: total,
          dateTime: timeStamp,
          products: cartProducts,
        ),
      );
    } catch (error) {
      print(error.toString());
    }
    notifyListeners();
  }
}
