import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class CartItem {
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,
  });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }

  int get itemQuantity {
    return _items.values.fold(
        0,
        (previousQuantity, currentItem) =>
            previousQuantity + currentItem.quantity);
  }

  double get totalAmount {
    return _items.values.fold(
        0,
        (previousQuantity, currentItem) =>
            previousQuantity + currentItem.price * currentItem.quantity);
  }

  void addItem(
    String productId,
    double price,
    String title,
  ) {
    if (_items.containsKey(productId)) {
      _items.update(
        productId,
        (cartItem) => CartItem(
          id: cartItem.id,
          title: cartItem.title,
          price: cartItem.price,
          quantity: cartItem.quantity + 1,
        ),
      );
    } else {
      _items.putIfAbsent(
        productId,
        () => CartItem(
          id: DateTime.now().toString(),
          title: title,
          price: price,
          quantity: 1,
        ),
      );
    }
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleUnit(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }
    if (_items[productId].quantity == 1) {
      this.removeItem(productId);
      return;
    }
    _items.update(
      productId,
      (currentItem) => CartItem(
        id: currentItem.id,
        price: currentItem.price,
        title: currentItem.title,
        quantity: currentItem.quantity - 1,
      ),
    );
    notifyListeners();
    return ;
  }

  void clearCart() {
    _items = {};
    notifyListeners();
  }
}
