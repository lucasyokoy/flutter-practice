import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

// import '../DUMMY_PRODUCTS.dart';
import '../providers/product.dart';
import '../models/http_exception.dart';

//with is a mixin. It's like extending a class. You inherit properties from the parent class
//however, unlike regular class extension, the instances of the extended classes don't also belong to the parent class. It just inherits the properties and methods.
//There is also a difference in the way the extendable classes and mixins are declared: class ExtendableCLass{}; mixin ExtendableMixin{}
//Also, Dart doesn't allow for extension of multiple classes, but it does allow for multiple mixins.
class ProductsProvider with ChangeNotifier {
  List<Product> _items; // = DummyProducts.products;
  final String authToken;
  final String userId;

  ProductsProvider(this.authToken, this.userId, this._items);

  List<Product> get items {
    // if (_showFavoritesOnly) {
    //   return _items
    //       .where(
    //         (product) => product.isFavorite,
    //       )
    //       .toList();
    // }
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items
        .where(
          (product) => product.isFavorite,
        )
        .toList();
  }

  Product findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }

  Future<void> fetchProducts() async {
    var databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userProducts/$userId.json?auth=$authToken';
    final response = await http.get(databaseURL);
    final responseData = json.decode(response.body) as Map<String, dynamic>;
    if (responseData == null) {
      _items = [];
      return;
    }
    if (response.statusCode >= 400) {
      throw HttpException('Failed to fetch products from database.');
    }
    databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userFavorites/$userId.json?auth=$authToken';
    final favoriteResponse = await http.get(databaseURL);
    final favoriteData = json.decode(favoriteResponse.body);
    final List<Product> loadedProducts = [];
    responseData.forEach(
      (productId, productData) {
        loadedProducts.add(
          Product(
            id: productId,
            title: productData['title'],
            description: productData['description'],
            price: productData['price'],
            imageUrl: productData['imageUrl'],
            //syntax for longer ternary expressions:
            //cond1 ? cond1 == true : cond2 == false ?? cond2 == null
            isFavorite:
                favoriteData == null ? false : favoriteData[productId] ?? false,
          ),
        );
      },
    );
    _items = loadedProducts;
    notifyListeners();
  }

  Future<void> addProduct(Product product) {
    //this first section communicates with the backend server
    //http.post demands you to parse the body of the request into JSON. json.encode() converts maps into JSON
    //without the .then, the frontend won't wait for the backend to process the request to continue the program
    //.then is execued imediatelly after the http.post receives a response. That's because http.post returns a Future Object.
    //in JavaScript, Futures are called Promises.
    //However, the code doesn't stop to wait. It moves on and, as soon as it detects that the Future has been completed, it jumps to the then function.
    //remember that .then() also returns a future. Therefore .then(...).then(...).then(...) is valid. This is called chaining
    //.catchError is executed in case the Future returns an error, instead of .then
    final databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userProducts/$userId.json?auth=$authToken';
    print('Added Product ${product.title}');
    return http
        .post(
      databaseURL,
      body: json.encode({
        'title': product.title,
        'description': product.description,
        'imageUrl': product.imageUrl,
        'price': product.price,
      }),
    )
        .then((response) {
      final newProduct = Product(
        title: product.title,
        description: product.description,
        imageUrl: product.imageUrl,
        price: product.price,
        id: json.decode(response.body)['name'],
      );
      _items.add(newProduct);
      //_items.insert(0,newProduct) would add the new product at the begining of the list
      notifyListeners();
    }).catchError((error) {
      print(error.toString());
      //throws the same error to whatever function will execute addProduct.
      throw error;
    });
  }
  // //async await provides a more readable syntax. See on http-05-async-await file. Works like in JavaScript
  // //when using async, the content of the function is automatically wrapped around a Future. so you don't need to return one.
  // //when a function is marked with await, flutter will wait for it's execution to finish and then move on.
  // //Thus, everything that isn't in an await behaves as if it was part of the .then() from the previous syntax
  // //If you want the response of the method, you can store it in a variable.
  // //for error handling, use the standard try catch finally;
  // Future<void> addProduct(Product product) async {
  //   const url = 'https://flutter-update.firebaseio.com/products';
  //   try {
  //     final response = await http.post(
  //       url,
  //       body: json.encode({
  //         'title': product.title,
  //         'description': product.description,
  //         'imageUrl': product.imageUrl,
  //         'price': product.price,
  //         'isFavorite': product.isFavorite,
  //       }),
  //     );
  //     final newProduct = Product(
  //       title: product.title,
  //       description: product.description,
  //       price: product.price,
  //       imageUrl: product.imageUrl,
  //       id: json.decode(response.body)['name'],
  //     );
  //     _items.add(newProduct);
  //     // _items.insert(0, newProduct); // at the start of the list
  //     notifyListeners();
  //   } catch (error) {
  //     print(error);
  //     throw error;
  //   }
  // }

  //here, we are using Optimistic Updating. That's when you keep a copy of the original product before deleting, so you can recover it in case of errors
  Future<void> deleteProduct(String productId) async {
    final databaseURL =
        'https://flutter-tutorial-fb707.firebaseio.com/userProducts/$userId/$productId.json?auth=$authToken';
    final existingProductIndex =
        _items.indexWhere((element) => element.id == productId);
    var existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    final response = await http.delete(databaseURL);
    if (response.statusCode >= 400) {
      //in case there is an error, catches it reinserts deleted product
      _items.insert(existingProductIndex, existingProduct);
      //remember that throw cancels the function execution and jumps to catch. It's like a return;
      //the exception should be handled at the child widget
      throw HttpException(
          'ERROR:${response.statusCode.toString()} could not delete product.');
    }
    existingProduct = null;
    notifyListeners();
  }

  void updateProduct(String id, Product newProduct) async {
    final productIndex = _items.indexWhere((product) => product.id == id);
    if (productIndex >= 0) {
      _items[productIndex] = newProduct;
      final databaseURL =
          'https://flutter-tutorial-fb707.firebaseio.com/userProducts/$userId/$id.json?auth=$authToken';
      final response = await http.patch(
        databaseURL,
        body: json.encode(
          {
            'title': newProduct.title,
            'description': newProduct.description,
            'imageUrl': newProduct.imageUrl,
            'price': newProduct.price,
            'isFavorite': newProduct.isFavorite,
          },
        ),
      );
      if (response.statusCode >= 400) {
        throw HttpException('Failed to Update Product');
      }
    } else {
      print('Product not found by updateProduct method from productsProvider');
    }
    notifyListeners();
  }
}
