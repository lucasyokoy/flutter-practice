import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//In case of multiple classes with the same name but different functions where both are necessary, the files can be imported with aliases
//ex: import '../providers/orders.dart' as ordersClass;
//import '../widgets/order_item_widget.dart' as ordersWidget;
//then whenever you call a class from these files use: ordersClass.orders.
import '../providers/orders.dart';
import '../providers/auth_provider.dart';
import '../widgets/order_item_widget.dart';
import '../widgets/app_drawer.dart';

class OrdersScreen extends StatelessWidget {
  static final routeName = '/screens/orders';

//without FutureBuilder, a StatefulWidget with all the commented out methods would be needed
//this makes the app overall more efficient because it doesn't rebuild the entire widget on every change, just the parts wrapped in Consumers, Providers and etc
//   @override
//   _OrdersScreenState createState() => _OrdersScreenState();
// }

// class _OrdersScreenState extends State<OrdersScreen> {
  // var _isLoading = false;

  // @override
  // void initState() {
  //   //a more elegeant approach would be to use didChangeDependencies, as in products_overview_screen.dart. This alternative approach was used for the sake of example.
  //   Future.delayed(Duration.zero).then((_) async {
  //     setState(() {
  //       _isLoading = true;
  //     });
  //     await Provider.of<Orders>(context, listen: false).fetchOrders();
  //     _isLoading = false;
  //   });

    // // because we're using listen: false, the Future.delayed workaround isn't needed, and teh code could be like this:
    // _isLoading = true;
    // Provider.of<Orders>(context, listen: false).fetchOrders().then((_) {
  //   //   setState(() {
  //   //     _isLoading = false;
  //   //   });
  //   // });

  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final authData = Provider.of<AuthProvider>(context);
    // final orderData = Provider.of<Orders>(context);
    //BE CAREFUL: if there is a method from a provider (with notifyListeners()) being called in the builder, like in FutureBUilder(builder:) and there's a listener in the same builder, this could trigger an infinite loop.
    //that's because Provider calls the build method again, when notifyListeners() is triggered
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Orders'),
      ),
      drawer: AppDrawer(),
      //FutureBuilder allows for the user of a StateLess widget with a
      body: FutureBuilder(
        future: Provider.of<Orders>(context, listen: false).fetchOrders(authData.userIdGetter),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (dataSnapshot.error != null) {
              return Center(
                child: Text('An Error Occurred!'),
              );
            } else {
              return Consumer<Orders>(
                builder: (ctx, orderData, child) {
                  return ListView.builder(
                    itemCount: orderData.orders.length,
                    itemBuilder: (ctx, index) {
                      return OrderItemWidget(orderData.orders[index]);
                    },
                  );
                },
              );
            }
          }
        },
      ),
      //this would be used to set a stateful widget
      // _isLoading
      //     ? Center(
      //         child: CircularProgressIndicator(),
      //       )
      //builder is the better choice because the amount of items in the list is unknown
      //     : ListView.builder(
      //         itemCount: orderData.orders.length,
      //         itemBuilder: (ctx, index) =>
      //             OrderItemWidget(orderData.orders[index]),
      //       ),
    );
  }
}
