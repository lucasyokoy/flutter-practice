import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/app_drawer.dart';
import '../widgets/products_grid.dart';
import '../widgets/badge.dart';
import '../providers/cart.dart';
import '../providers/products_provider.dart';
import '../screens/cart_screen.dart';

enum FilterOptions {
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showOnlyFavorites = false;
  var _isFirstRun = true;
  var _isLoading = true;
  var _loadingError = false;
  var _loadingException;

  @override
  void initState() {
    //remember that when initState is run, the context doesn't exit yet.
    //that is why it would need to happen as a Future after the code moves on and the context has been created.
    //an alternatives solution is to use didChangeDependencies() as see below.
    // Future.delayed(Duration.zero).then((_){
    //   Provider.of<ProductsProvider>(context).initialFetch();
    // });
    super.initState();
  }

  //this is executed before the build method runs, every time it's called. So, it happens after the context has been created
  //however, it runs multiple times (everytime right before build runs). Because I want it to only run in the first run, I need that _isFirstRun variable
  // @override
  // void didChangeDependencies() {
  //   if (_isFirstRun) {
  //     setState(() {
  //       _isLoading = true;
  //     });
  //     Provider.of<ProductsProvider>(context).initialFetch().then((_) {
  //       //remember to call setState after fetching data or the changes won't be reflected on the interface.
  //       setState(() {
  //         _isLoading = false;
  //       });
  //       _isFirstRun = false;
  //       super.didChangeDependencies();
  //     });
  //   }
  // }
  @override
  void didChangeDependencies() {
    if (_isFirstRun) {
      setState(() {
        _isLoading = true;
        _loadingError = false;
      });
      Provider.of<ProductsProvider>(context).fetchProducts().then((_) {
        setState(() {
          _isLoading = false;
        });
      }).catchError((onError) {
        setState(() {
          _loadingException = onError.toString();
          print(_loadingException);
          _isLoading = false;
          _loadingError = true;
        });
      });
    }
    _isFirstRun = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    //final productData = Provider.of<ProductsProvider>(context);
    //this state could be managed with a product provider. But using a stateful widget is a better choice, otherwise the bool isFavorite would affect all widgets listening to the provider, not just this particular widget
    return Scaffold(
      appBar: AppBar(
        title: Text('Products Overview'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              setState(
                () {
                  if (selectedValue == FilterOptions.Favorites) {
                    _showOnlyFavorites = true;
                  } else {
                    _showOnlyFavorites = false;
                  }
                },
              );
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text('Favorites'),
                value: FilterOptions.Favorites,
              ),
              PopupMenuItem(
                child: Text('Show All'),
                value: FilterOptions.All,
              ),
            ],
          ),
          Consumer<Cart>(
            builder: (_, cart, iconButton) => Badge(
              child: iconButton,
              value: cart.itemCount.toString(),
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
          ),
        ],
      ),
      //works similarly to ListView.builder() and ListView()
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : _loadingError
              ? Text(_loadingException)
              : ProductsGrid(_showOnlyFavorites),
      drawer: AppDrawer(),
    );
  }
}
