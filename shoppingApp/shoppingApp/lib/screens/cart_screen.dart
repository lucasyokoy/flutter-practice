import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/cart_item_widget.dart';
import '../providers/cart.dart';
import '../providers/orders.dart';
import '../providers/auth_provider.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/screens/cart_screen';
  @override
  Widget build(BuildContext context) {
    final userId = Provider.of<AuthProvider>(context).userIdGetter;
    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Total",
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Chip(
                    label: Text(
                      '\$${cart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color:
                            Theme.of(context).primaryTextTheme.headline6.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  OrderButton(cart: cart, userId: userId),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView.builder(
              itemCount: cart.itemCount,
              itemBuilder: (ctx, index) =>
                  //remember that cart.items returns a map, not a list; Cart.items.values returns an iterable with the values, but still not a list.
                  CartItemWidget(
                cart.items.values.toList()[index],
                cart.items.keys.toList()[index],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OrderButton extends StatefulWidget {
  const OrderButton({
    @required this.cart,
    @required this.userId,
  });

  final Cart cart;
  final String userId;

  @override
  _OrderButtonState createState() => _OrderButtonState();
}

class _OrderButtonState extends State<OrderButton> {
  var _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: Theme.of(context).textTheme.headline6.color,
      onPressed: (widget.cart.totalAmount <= 0) || _isLoading
          ? null
          : () async {
              setState(() {
                _isLoading = true;
              });
              //if the cart is empty, dismiss button pressing
              if (widget.cart.items.isNotEmpty)
                //listen: false because I don't want anything to change, i'm just dispatching an action
                await Provider.of<Orders>(
                  context,
                  listen: false,
                ).addOrder(
                  widget.cart.items.values.toList(),
                  widget.cart.totalAmount,
                  widget.userId,
                );
              setState(() {
                _isLoading = false;
              });
              widget.cart.clearCart();
            },
      child: _isLoading ? CircularProgressIndicator() : Text('ORDER NOW'),
    );
  }
}
