//it's not recommended to create custom exceptions on files or widgets. It's good pratice to create a specific file for this (for more details, hover over Exception name)
//implements is like in Java: implements an abstract Class
class HttpException implements Exception{
  final String message;

  HttpException(this.message);

  @override
  String toString() {
    return message;
  }
}