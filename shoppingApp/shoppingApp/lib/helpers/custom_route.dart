import 'package:flutter/material.dart';

class CustomRoute<T> extends MaterialPageRoute<T> {
  //super redirects the info for the parent class
  CustomRoute({
    WidgetBuilder builder,
    RouteSettings settings,
  }) : super(
          builder: builder,
          settings: settings,
        );

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    //if it's the initial route I don't want to animate it
    if(settings.name == '/'){
      return child;
    }
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}

//this class has to be created so we can use it in the main.dart file
//CustomRoute can't be used because it's a route, not only a transition builder
class CustomPageTransitionBuilder extends PageTransitionsBuilder{
  @override
  Widget buildTransitions<T>(
    PageRoute<T> route,
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    //if it's the initial route I don't want to animate it
    if(route.settings.name == '/'){
      return child;
    }
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}