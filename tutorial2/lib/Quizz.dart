import 'package:flutter/material.dart';

import './ButtonContainer.dart';

class Quizz extends StatelessWidget {
  final Function selectAlternative;
  final String question;
  final List<Map> alternatives;

  Quizz(
      {this.question,
      this.alternatives,
      this.selectAlternative});

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      //Question
      Text(question),
      //_alternatives
      buildListOfAlternatives(selectAlternative),
    ]);
  }

  Widget buildListOfAlternatives(Function selectAlternative) {
    return Column(
      children: [
        ...(alternatives as List<Map>)
            .map((alternative)=>ButtonContainer(alternative['text'], alternative['score'], selectAlternative))
            .toList(),
      ],
    );
  }
}
