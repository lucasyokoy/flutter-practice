import 'package:flutter/material.dart';

class EndofQuizzPage extends StatelessWidget {
  final Function restartQuizz;
  final int totalScore;

  EndofQuizzPage(this.restartQuizz, this.totalScore);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text('Your total score was:'),
          Text(totalScore.toString()),
          RaisedButton(
            onPressed: restartQuizz,
            child: Text('Retake Quizz?'),
          )
        ],
      ),
    );
  }
}
