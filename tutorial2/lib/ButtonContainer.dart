import 'package:flutter/material.dart';

class ButtonContainer extends StatelessWidget {
  final String text;
  final int score;
  final Function selectAlternative;

  ButtonContainer(this.text, this.score, this.selectAlternative);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10),
      child:
        RaisedButton(
          onPressed: ()=>selectAlternative(score),
          child: Text(text),
        ),
    );
  }
}
