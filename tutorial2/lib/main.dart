import 'package:flutter/material.dart';

import './Quizz.dart';
import './EndofQuizzPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  int _questionNumber = 0;
  int _totalScore = 0;
  bool endofQuizz = false;
  final List<String> _questions = ['Question 1', 'Question 2', 'Question 3'];
  final List<List<Map>> _alternatives = [
    [
      {'text':'Answer 1 a','score':10},
      {'text':'Answer 1 b','score':20},
    ],
    [
      {'text':'Answer 2 a','score':50},
      {'text':'Answer 2 b','score':20},
      {'text':'Answer 2 c','score':40},
    ],
    [
      {'text':'Answer 3 a','score':30},
      {'text':'Answer 3 b','score':90},
      {'text':'Answer 3 c','score':70},
      {'text':'Answer 3 d','score':40},
    ]
  ];

  void _selectAlternative(int score) {
    setState(() {
      _questionNumber++;
      _totalScore += score;
      if (_questionNumber >= _questions.length) {
        endofQuizz = true;
      }
    });
  }

  void _restartQuizz() {
    setState(() {
      _totalScore = 0;
      _questionNumber = 0;
      endofQuizz = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quizz'),
        ),
        body: endofQuizz
          ? EndofQuizzPage(_restartQuizz, _totalScore)
          : Quizz(
              question: _questions[_questionNumber],
              alternatives: _alternatives[_questionNumber],
              selectAlternative: _selectAlternative)),
    );
  }
}
