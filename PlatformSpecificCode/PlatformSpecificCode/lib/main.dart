import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Use Native Code to Get Battery Level',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _batteryLevel;

  Future<void> _getBatteryLevel() async {
    //MethodChannel should receive a unique identifier as an argument. The convention is to use an URL like string, but anything goes
    const platform = MethodChannel('course.flutter.dev/battery');
    try {
      //tries to call the getBatteryLevel from the native device
      //the getBatteryLevel function is defined at .\android\app\src\main\java\com\example\PlatformSpecificCode\MainActivity.java
      final batteryLevel = await platform.invokeMethod('getBatteryLevel');
      setState((){
      _batteryLevel = batteryLevel;
    });
    } on PlatformException catch (error) {
      _batteryLevel = null;
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
    _getBatteryLevel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Native Code'),
      ),
      body: Center(
        child: Text('Battery Level: $_batteryLevel'),
      ),
    );
  }
}
