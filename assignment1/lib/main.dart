/*
1) Create a new Flutter App (in this project) and output an AppBar
and some text below it
2) Add a butotn which changes the text to any other text 
of your choice.
3) Split the app into three widgets: App, TextControl & Text
*/
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

//main:(stateless)
//AppBar
//AppBar Theme:
  //brightness: dark
//Text (mutable)

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Swap Text App'),
        ),
        body: TextManager(),
      )
    );
  }
}

//TextManager:(stateful)
//Text1 and Text2 in a list
//Widget with text to be displayed
//Button (changes text when pressed)
//switches between texts 1 and 2 when button is pressed and rebuild
class TextManager extends StatefulWidget {
  final List<String> phrases = ['Congratulations!','You did it!'];

  @override
  State<StatefulWidget> createState() {
    return _TextManagerState();
  }
}

class _TextManagerState extends State<TextManager>{
  int _state = 0;
  @override
  Widget build(BuildContext context){
    return Center(child:Column(children:[
      //Text
      Text(widget.phrases[_state]),
      //Button
      Container(
        margin: EdgeInsets.all(10.0),
        child: RaisedButton(
          color: Colors.lightBlue,
          onPressed: () {
            setState(() {
              _state = switchState(_state);
            });
          },
          child: Text('Change Text'),
        ),
      ),
    ],),);
  }

  int switchState(int currentState){
    int newState = 0;
    if(currentState == 0){
      newState=1;
    }
    return newState;
  }
}